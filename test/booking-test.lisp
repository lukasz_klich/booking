(in-package :cl-user)

(defpackage :biz.apeskull.booking-test
  (:use :common-lisp
        :lisp-unit
        :biz.apeskull.booking)
  (:import-from :biz.apeskull.booking
                :in-range-p
                :free-appointments-inter
                :filter-hour
                :ranges-overlaps-p
                :appointments-collides-p
                :make-appointment
                :new-appointment-collides-p))

(in-package :biz.apeskull.booking-test)

(define-test in-range-p-test
  (assert-true (in-range-p 1 0 2))
  (assert-true (in-range-p -5 -10 -2))
  (assert-false (in-range-p 10 1 2))
  (assert-true (in-range-p 3 5 2))
  (assert-false (in-range-p 0 1 2)))

(define-test ranges-overlaps-p-test
  (assert-true (ranges-overlaps-p 0 2 1 3))
  (assert-true (ranges-overlaps-p -2 0 -3 -1))
  (assert-false (ranges-overlaps-p 0 2 3 4))
  (assert-false (ranges-overlaps-p -4 -3 -2 -1))
  (assert-true (ranges-overlaps-p -1 -4 -2 -3)))

(define-test appointments-collides-p-test
  (assert-true (appointments-collides-p (make-appointment-with-range 10 12) (make-appointment-with-range 9 11)))
  (assert-true (appointments-collides-p (make-appointment-with-range 10 12) (make-appointment-with-range 10 12)))
  (assert-true (appointments-collides-p (make-appointment-with-range 11 13) (make-appointment-with-range 10 12)))
  (assert-true (appointments-collides-p (make-appointment-with-range 11 13) (make-appointment-with-range 11.5 12)))
  (assert-false (appointments-collides-p (make-appointment-with-range 10 12) (make-appointment-with-range 8 10)))
  (assert-false (appointments-collides-p (make-appointment-with-range 11 13) (make-appointment-with-range 13 15)))
  (assert-false (appointments-collides-p (make-appointment-with-range 11 13) (make-appointment-with-range 8 10))))

(define-test free-appointments-inter-test
  (let ((appointments (make-appointments-list)))
  (assert-equalp '(9 9.5 12 12.5)
                (free-appointments-inter appointments 8 13))
  (assert-equalp '()
                (free-appointments-inter appointments 10 12))
  (assert-equalp '(6 6.5 9 9.5)
                (free-appointments-inter appointments 6 10))
  (assert-equalp '(6 6.5 9 9.5 12 12.5)
                (free-appointments-inter appointments 6 13))))

(define-test filter-hour-test
  (let ((appointments (make-appointments-list)))
    (let ((filtering-p (filter-hour appointments)))
      (assert-equal nil (funcall filtering-p 9.5))
      (assert-equal t (funcall filtering-p 11))
      (assert-equal nil (funcall filtering-p 6))
      (assert-equal nil (funcall filtering-p 13)))))

(defun make-appointment-with-range (from to)
  (make-appointment "" "" from to "" "" ""))

(defun make-appointments-list ()
  (concatenate 'list
               (list (make-appointment-with-range 10 12))
               (list (make-appointment-with-range 7 9))))

(define-test new-appointment-collides-p-test
    (let ((appointments (make-appointments-list)))
      (assert-false (new-appointment-collides-p
                     appointments (make-appointment-with-range 9 10)))
      (assert-false (new-appointment-collides-p
                     appointments (make-appointment-with-range 6 7)))
      (assert-false (new-appointment-collides-p
                     appointments (make-appointment-with-range 12 13)))
      (assert-true (new-appointment-collides-p
                    appointments (make-appointment-with-range 6 14)))
      (assert-true (new-appointment-collides-p
                    appointments (make-appointment-with-range 10 11)))
      (assert-true (new-appointment-collides-p
                    appointments (make-appointment-with-range 8 11)))))

(setf *print-failures* t)
(setf *print-errors* t)

(run-tests)
