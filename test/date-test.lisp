(in-package :cl-user)

(defpackage :biz.apeskull.booking.date-test
  (:use :common-lisp
        :lisp-unit
        :biz.apeskull.booking)
  (:import-from :biz.apeskull.booking.date
                :day-of-week-in-month
                :days-in-month
                :leap-year-p))

(in-package :biz.apeskull.booking.date-test)

(define-test day-of-week-in-month-test ()
             (assert-equal '(4 11 18 25)
                           (day-of-week-in-month 2 2013 0))
             (assert-equal '(1 8 15 22)
                           (day-of-week-in-month 2 2013 4))
             (assert-equal '(3 10 17 24 31)
                           (day-of-week-in-month 3 2013 6))
             (assert-equal '(7 14 21 28)
                           (day-of-week-in-month 8 2012 1))
             (assert-equal '(1 8 15 22 29)
                           (day-of-week-in-month 8 2012 2))
             (assert-equal '(3 10 17 24 31)
                           (day-of-week-in-month 8 2012 4))
             (assert-equal '(4 11 18 25)
                           (day-of-week-in-month 8 2012 5)))

(define-test leap-year-p-test ()
             (assert-true (leap-year-p 2000))
             (assert-true (leap-year-p 2004))
             (assert-true (leap-year-p 2096))
             (assert-false (leap-year-p 2100))
             (assert-false (leap-year-p 2001))
             (assert-false (leap-year-p 2002))
             (assert-false (leap-year-p 2003)))

(define-test days-in-month-test ()
             (assert-equal 28 (days-in-month 2013 2))
             (assert-equal 31 (days-in-month 2013 3))
             (assert-equal 30 (days-in-month 2013 4))
             (assert-equal 31 (days-in-month 2013 5))
             (assert-equal 30 (days-in-month 2013 6))
             (assert-equal 29 (days-in-month 2016 2))
             (assert-equal 29 (days-in-month 2016 2))
             (assert-equal 29 (days-in-month 2000 2))
             (assert-equal 28 (days-in-month 2100 2)))

(setf *print-failures* t)
(setf *print-errors* t)

(run-tests)
