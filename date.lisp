(in-package :cl-user)

(defpackage :biz.apeskull.booking.date
  (:use :common-lisp)
  (:export :day-of-week-in-month))

(in-package :biz.apeskull.booking.date)


(defun day-of-week-in-month (month year day-of-week)
  (let ((days (loop
                 for day from 1 to (days-in-month year month)
                 collect day)))
    (remove-if-not
     #'(lambda (day) (= day-of-week (day-of-week day month year)))
     days)))

(defun day-of-week (day month year)
  "Returns the day of the week as an integer.
Monday is 0."
  (declare (type (and fixnum (integer 1 31)) day)
           (type (and fixnum (integer 1 12)) month)
           (type (and fixnum (integer 0)) year))
  (nth-value
   6
   (decode-universal-time
    (encode-universal-time 0 0 0 day month year 0)
    0)))

(defun days-in-month (year month)
  (declare (type (and fixnum (integer 1 12)) month)
           (type (and fixnum (integer 0)) year))
  (cond ((or (= month 9) (= month 10) (= month 4) (= month 6)) 30)
        ((= month 2) (if (leap-year-p year) 29 28))
        (t 31)))

(defun leap-year-p (year)
  (or (and (divisible-p year 4) (not (divisible-p year 100)))
      (divisible-p year 400)))

(defun divisible-p (number divisor)
  (= 0 (mod number divisor)))
