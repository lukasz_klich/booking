(in-package :cl-user)

(defpackage :biz.apeskull.booking.config
  (:use :common-lisp)
  (:export :config))

(in-package :biz.apeskull.booking.config)

(defparameter *config* (make-hash-table))
(setf (gethash :day-beginning *config*) 8)
(setf (gethash :day-end *config*) 18)
(setf (gethash :working-days *config*) '(0 2 4))

(defun config (key) "Gets value from configuration"
  (gethash key *config*))
