(in-package :cl-user)

(defpackage :biz.apeskull.booking.db
  (:use :common-lisp
        :alexandria
        :cl-mongo)
  (:export :get-from-db
           :save-to-db
           :make-date)
  (:import-from :cl-mongo
                :elements
                :ht->document))

(in-package :biz.apeskull.booking.db)

(db.use "calendar")

(defvar *time-zone* (time-zone))

(defun get-from-db (collection key value)
  (let ((docs (docs (iter (db.find collection ($ key value) :limit 0)))))
    (loop for doc in docs collect (document->ht doc))))

(defun save-to-db (collection ht)
    (db.save (string-downcase collection) (keyword-ht->document ht)))

(defun document->ht (doc)
  (copy-hash-table (elements doc)))

(defun keyword-ht->document (ht)
  (let ((doc (make-document)))
    (loop for key being the hash-keys of ht
         do (add-element (string-downcase key) (gethash key ht) doc))
    doc))

(defun make-date (day month year &optional time-zone)
  (date-time 0 0 0 day month year time-zone))
