(defpackage #:biz.apeskull.booking-config (:export #:*base-directory*))
(defparameter biz.apeskull.booking-config:*base-directory*
  (make-pathname :name nil :type nil :defaults *load-truename*))

(asdf:defsystem #:booking
  :serial t
  :description "Booking system for appointments in my wife's groomer shop"
  :author "Łukasz Klich <klich.lukasz@gmail.com>"
  :depends-on (:RESTAS
               :CL-MONGO
               :CL-JSON
               :ALEXANDRIA)
  :components ((:file "config")
               (:file "db")
               (:file "date")
               (:file "booking")
               (:file "urls")))

(asdf:defsystem #:booking-test
  :author "Łukasz Klich <klich.lukasz@gmail.com>"
  :licence "MIT"
  :description "Tests for booking system"
  :depends-on (:BOOKING
               :LISP-UNIT)
  :serial t
  :components
  ((:module "test"
    :serial t
    :components ((:file "booking-test")
                 (:file "date-test")))))
