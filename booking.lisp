(in-package :cl-user)

(defpackage :biz.apeskull.booking
  (:nicknames :ape-book)
  (:use :common-lisp
        :biz.apeskull.booking.db
        :biz.apeskull.booking.config
        :biz.apeskull.booking.date)
  (:export :free-appointments
           :add-appointment
           :make-appointment
           :work-days))

(in-package :biz.apeskull.booking)

(defun appointments (date)
  "Returns all appointments book for day"
  (get-from-db "appointments" "date" date))
;; #'(lambda (a1 a2)
;;     (print (gethash :from a1))
;;     (print (gethash :from a1))
;;     (< (gethash :from a1) (gethash :from a2)))))

(defun free-appointments-inter (appointments start end)
  "Pure function. Internal implementation of free-appointments."
  (let* ((step 0.5)
         (end (- end step))
         (start (float start)))
    (let ((hours
           (loop for hour from start to end by step collect hour)))
      (let ((remove-p (filter-hour appointments)))
        (remove-if remove-p hours)))))

(defun filter-hour (appointments)
  "Returns predicate that is false if hour collides with
any appointment in appointments"
  #'(lambda (hour)
      (loop for a in appointments thereis
           (in-range-p hour (get-value "from" a) (get-value "to" a)))))


(defun free-appointments (day month year)
  "Returns avalaible free hours to book for provided day of the year"
  (let ((appointments (appointments (make-date day month year))))
    (free-appointments-inter appointments
                             (config :day-beginning)
                             (config :day-end))))

(defun work-days (month year)
  "Returns all days in month that groom shop will be open"
  (let ((working-days (config :working-days)))
    (sort (loop for day-of-week in working-days
               append (day-of-week-in-month month year day-of-week))
          #'<)))

(defun add-appointment (worker day month year from to phone type client)
  "Adds new appointment if it's not colliding with any other appointment"
  (add-appointment-inter (make-appointment worker
                                           (make-date day month year)
                                           from
                                           to
                                           phone
                                           type
                                           client)))

(defun add-appointment-inter (appointment)
  (let ((appointments (appointments (get-value "date" appointment))))
    (if (or (not appointments)
            (not (new-appointment-collides-p appointments appointment)))
        (save-to-db "appointments" appointment)
        (error 501))))

(defun new-appointment-collides-p (day-plan appointment)
  "Checks if new appointment collides with list of already booked"
  (reduce #'(lambda (x y) (or x y))
          (mapcar #'(lambda (m) (appointments-collides-p m appointment)) day-plan)))

(defun appointments-collides-p (meet1 meet2)
  "Checks if two meetings are colliding"
  (ranges-overlaps-p (get-value "from" meet1) (get-value "to" meet1)
                     (get-value "from" meet2) (get-value "to" meet2)))

(defun make-range (start end)
  (list (min start end) (max start end)))

(defun range-start (range)
  (first range))

(defun range-end (range)
  (second range))

(defun ranges-overlaps-p (s1 e1 s2 e2)
  "Checks if two ranges are overlaping"
  (or (in-range-p s1 s2 e2)
      (in-range-p s2 s1 e1)))

(defun in-range-p (num start end)
  "Checks if number is beetwen start and end"
  (and (>= num (min start end))
       (< num (max start end))))


(defun get-value (key appointment)
  "Gets value for provided key from appointment"
  (gethash key appointment))


(defun make-appointment (worker date from to phone type client)
  "Creates appointment"
  (let ((doc (make-hash-table :test #'equalp)))
    (setf (gethash "worker" doc) worker)
    (setf (gethash "date" doc) date)
    (setf (gethash "from" doc) from)
    (setf (gethash "to" doc) to)
    (setf (gethash "phone" doc) phone)
    (setf (gethash "type" doc) type)
    (setf (gethash "client" doc) client)
    doc))
