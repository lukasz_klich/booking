(in-package :cl-user)

(defpackage :biz.apeskull.booking.db
  (:use :common-lisp
        :cl-mongo))

(in-package :biz.apeskull.booking.db)
