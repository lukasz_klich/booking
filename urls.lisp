(in-package :cl-user)

(restas:define-module #:biz.apeskull.booking.urls
  (:use :common-lisp
        :cl-json))

(in-package #:biz.apeskull.booking.urls)

(defun as-json (func &rest args)
  (format nil "~a" (cl-json:encode-json-to-string (apply func args))))

(restas:define-route get-free-appointments ("free-appointments/:year/:month/:day-of-month"
                                        :method :get
                                        :content-type "application/json")
  (let ((day-of-month (parse-integer day-of-month :junk-allowed t))
        (month (parse-integer month :junk-allowed t))
        (year (parse-integer year :junk-allowed t)))
    (as-json #'ape-book:free-appointments day-of-month month year)))

(restas:define-route get-work-days ("work-days/:year/:month"
                                        :method :get
                                        :content-type "application/json")
  (let ((month (parse-integer month :junk-allowed t))
        (year (parse-integer year :junk-allowed t)))
    (as-json #'ape-book:work-days month year)))


;; (restas:define-route book-appointment ("add"
;;                                 :method :post)
;;   (let ((worker (hunchentoot:post-parameter "worker"))
;;         (year (hunchentoot:post-parameter "year"))
;;         (month (hunchentoot:post-parameter "month"))
;;         (day (hunchentoot:post-parameter "day"))
;;         (from (hunchentoot:post-parameter "from"))
;;         (to (hunchentoot:post-parameter "to"))
;;         (phone (hunchentoot:post-parameter "phone"))
;;         (type (hunchentoot:post-parameter "type"))
;;         (client (hunchentoot:post-parameter "client")))
;;     (print worker)
;;     (print day)
;;     (print month)
;;     (print year)
;;     (print from)
;;     (print to)
;;     (print phone)
;;     (print type)
;;     (print client)
;;     (format nil "~a" (add-appointment worker day month year from to phone type client))))

(restas:start '#:biz.apeskull.booking.urls :port 8081)
